#!/usr/bin/env bash

VER="1.4"

# Translation
source /usr/bin/recbox-nightlight_lang.sh

SETTINGS_PATH="$HOME/.config/recbox-nightlight-settings/config"

if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
    echo -e ":: sidemenu directory don't exist\n -> skipping"
else
    FONT_ICON_ON=$(grep -w 'font_icon_on' "$HOME/.config/recbox-nightlight-settings/config" | awk -F '=' '{print $2}')
    FONT_ICON_OFF=$(grep -w 'font_icon_off' "$HOME/.config/recbox-nightlight-settings/config" | awk -F '=' '{print $2}')
    SIDEMENU_PATH="$HOME/.config/side-menu/sidemenu-config.csv"
fi

config_test() {
create_new_config() {
touch "$SETTINGS_PATH"
echo -e "redshift_status=Off
font_icon_on=
font_icon_off=
color_temp=4500
brightness=1.0
gamma=1.0
method=randr" > "$SETTINGS_PATH"
}
    if [[ -z $(find "$HOME/.config" -type d -name recbox-nightlight-settings) ]]; then
        mkdir "$HOME/.config/recbox-nightlight-settings"
        create_new_config
    elif [[ -z $(find "$HOME/.config/recbox-nightlight-settings" -type f -name config) ]]; then
        create_new_config
    elif [[ ! -s $SETTINGS_PATH ]]; then
        create_new_config
    fi
}

run() {
STATUS=$(grep -w 'redshift_status' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
COLOR_TEMP=$(grep -w 'color_temp' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
BRIGHTNESS=$(grep -w 'brightness' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
GAMMA=$(grep -w 'gamma' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
METHOD=$(grep -w 'method' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
    if [[ $STATUS == "On" ]]; then
        redshift -x -m "$METHOD"
        sed -i "/redshift_status/ s/$STATUS/Off/" "$SETTINGS_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
            echo -e ":: sidemenu config don't exist\n -> skipping"
        else
            sed -i "s/$FONT_ICON_ON/$FONT_ICON_OFF/" "$SIDEMENU_PATH"
        fi
    elif [[ $STATUS == "Off" ]]; then
        redshift -O "$COLOR_TEMP" -g "$GAMMA" -b "$BRIGHTNESS" -m "$METHOD" -P
        sed -i "/redshift_status/ s/$STATUS/On/" "$SETTINGS_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
            echo -e ":: sidemenu config don't exist\n -> skipping"
        else
            sed -i "s/$FONT_ICON_OFF/$FONT_ICON_ON/" "$SIDEMENU_PATH"
        fi
    else
        echo "error"
    fi
}

restart() {
COLOR_TEMP=$(grep -w 'color_temp' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
BRIGHTNESS=$(grep -w 'brightness' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
GAMMA=$(grep -w 'gamma' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
METHOD=$(grep -w 'method' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
    redshift -O "$COLOR_TEMP" -g "$GAMMA" -b "$BRIGHTNESS" -m "$METHOD" -P
}

reset_status() {
STATUS=$(grep -w 'redshift_status' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
    if [[ $STATUS == "On" ]]; then
        sed -i "/redshift_status/ s/$STATUS/Off/" "$SETTINGS_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
            echo -e ":: sidemenu config don't exist\n -> skipping"
        else
            sed -i "s/$FONT_ICON_ON/$FONT_ICON_OFF/" "$SIDEMENU_PATH"
        fi
    else
        echo "error"
    fi
}

settings() {
SEPARATOR=""
SPACE=""

ICON_PATH="/usr/share/icons/hicolor/64x64/apps/recbox-nightlight-settings.svg"
TEMP_WIDGET_VALUE=$(grep -w 'color_temp' "$SETTINGS_PATH" | awk -F '=' '{print $2}')
BRGHT_WIDGET_VALUE=$(grep -w 'brightness' "$SETTINGS_PATH" | awk -F '.' '{print $2}')
GAMMA_WIDGET_VALUE=$(grep -w 'gamma' "$SETTINGS_PATH" | awk -F '.' '{print $2}')
STATUS=$(grep -w 'redshift_status' "$SETTINGS_PATH" | awk -F '=' '{print $2}')

ZEN_MENU="zenity --list --width=512 --height=240"
TEMP_WIDGET="zenity --scale --min-value=3000 --max-value=6500 --step=1"
BRGHT_WIDGET="zenity --scale --min-value=1 --max-value=10 --step=10"

    BOX=$($ZEN_MENU --title="$MAIN_MENU_TITLE" --text="$MAIN_MENU_TEXT" \
    --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
    --column="$COLUMN_1" --column="$SEPARATOR" --column="$COLUMN_2" \
    "$SETTINGS_OPTION_1" "$SPACE" "$SETTINGS_OPTION_1_DESC" \
    "$SETTINGS_OPTION_2" "$SPACE" "$SETTINGS_OPTION_2_DESC" \
    "$SETTINGS_OPTION_3" "$SPACE" "$SETTINGS_OPTION_3_DESC" \
    "$SETTINGS_OPTION_4" "$SPACE" "$SETTINGS_OPTION_4_DESC" \
    --window-icon="$ICON_PATH")
    if [[ $BOX == "$SETTINGS_OPTION_1" ]]; then
        TEMP_BOX=$($TEMP_WIDGET --value="$TEMP_WIDGET_VALUE" \
        --window-icon="$ICON_PATH" --title="$TEMP_WIDGET_TITLE" \
        --text="$TEMP_WIDGET_TEXT" --ok-label="$OK_BUTTON" \
        --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON")
        if [[ -z $TEMP_BOX ]]; then
            exit
        elif [[ $TEMP_BOX == "$EXTRA_BUTTON" ]]; then
            settings
        elif [[ $TEMP_BOX == "$TEMP_BOX" ]]; then
            sed -i "/color_temp/ s/$TEMP_WIDGET_VALUE/$TEMP_BOX/" "$SETTINGS_PATH"
            if [[ $STATUS == "On" ]]; then
                restart && settings
            elif [[ $STATUS == "Off" ]]; then
                settings
            fi
        fi
    elif [[ $BOX == "$SETTINGS_OPTION_2" ]]; then
        if [[ $BRGHT_WIDGET_VALUE -eq 0  ]]; then
            BRGHT_WIDGET_VALUE="10"
        fi
        BRGHT_BOX=$($BRGHT_WIDGET --value="$BRGHT_WIDGET_VALUE" \
        --window-icon="$ICON_PATH" --title="$BRGHT_WIDGET_TITLE" \
        --text="$BRGHT_WIDGET_TEXT" --ok-label="$OK_BUTTON" \
        --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON")
        if [[ -z $BRGHT_BOX ]]; then
            exit
        elif [[ $BRGHT_BOX == "$EXTRA_BUTTON" ]]; then
            settings
        elif [[ $BRGHT_BOX -eq 10 ]]; then
            sed -i "s/$(grep brightness "$SETTINGS_PATH")/brightness=1.0/" "$SETTINGS_PATH"
            if [[ $STATUS == "On" ]]; then
                restart && settings
            elif [[ $STATUS == "Off" ]]; then
                settings
            fi
        elif [[ $BRGHT_BOX -lt 10 ]]; then
            sed -i "s/$(grep brightness "$SETTINGS_PATH")/brightness=0.$BRGHT_BOX/" "$SETTINGS_PATH"
            if [[ $STATUS == "On" ]]; then
                restart && settings
            elif [[ $STATUS == "Off" ]]; then
                settings
            fi
        fi
    elif [[ $BOX == "$SETTINGS_OPTION_3" ]]; then
        if [[ $GAMMA_WIDGET_VALUE -eq 0  ]]; then
            GAMMA_WIDGET_VALUE="10"
        fi
        GAMMA_BOX=$($BRGHT_WIDGET --value="$GAMMA_WIDGET_VALUE" \
        --window-icon="$ICON_PATH" --title="$GAMMA_WIDGET_TITLE" \
        --text="$GAMMA_WIDGET_TEXT" --ok-label="$OK_BUTTON" \
        --cancel-label="$CANCEL_BUTTON" --extra-button="$EXTRA_BUTTON")
        if [[ -z $GAMMA_BOX ]]; then
            exit
        elif [[ $GAMMA_BOX == "$EXTRA_BUTTON" ]]; then
            settings
        elif [[ $GAMMA_BOX -eq 10 ]]; then
            sed -i "s/$(grep gamma "$SETTINGS_PATH")/gamma=1.0/" "$SETTINGS_PATH"
            if [[ $STATUS == "On" ]]; then
                restart && settings
            elif [[ $STATUS == "Off" ]]; then
                settings
            fi
        elif [[ $GAMMA_BOX -lt 10 ]]; then
            sed -i "s/$(grep gamma "$SETTINGS_PATH")/gamma=0.$GAMMA_BOX/" "$SETTINGS_PATH"
            if [[ $STATUS == "On" ]]; then
                restart && settings
            elif [[ $STATUS == "Off" ]]; then
                settings
            fi
        fi
    elif [[ $BOX == "$SETTINGS_OPTION_4" ]]; then
        zenity --info --no-wrap --icon-name="recbox-nightlight-settings" \
        --window-icon="$ICON_PATH" --title="$ABOUT_WIDGET_TITLE" \
        --text="<b>Version: $VER</b>\n\n$ABOUT_WIDGET_TEXT"
        settings
    fi
}

case "$1" in
    --run | -r ) config_test; run;;
    --reset-status | -R ) reset_status;;
    --settings | -s ) config_test; settings;;
    --version | -v ) echo -e "\n    Version $VER\n";;
    *)
echo -e "
    Usage:\n
    recbox-nightlight.sh [ OPTION ]\n
    Options:\n
     -r, --run               Run Redshift in one-shot mode.
     -R, --reset-status      Reset Night Light status on Shutdown, Restart,
                             Switch User, Log Out.
     -s, --settings          Open Night Light Settings menu.
     -v, --version           Show script version.
" >&2
exit 1;;
esac
exit 0
